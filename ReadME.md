
# 작업용 VM 생성하기 
### IBM CLOUD에서 VM생성 
centos8을 선택하여 vm을 생성한다. 


### VM yum 초기화 
최초에 한번 yum을 업데이트 한다.
```
yum -y update
```

### host명 변경하기 
host명 변경

hostname 으로 확인
```
# hostname 변경하기 
[root@virtualserver01 ~]# hostnamectl set-hostname 169.56.76.164.nip.io

# hostmane 확인하기   
[root@virtualserver01 ~]# hostname
169.56.76.164.nip.io
```

vi /etc/host 에서 변경 
```
```

# docker 설치하기 
### centos8에 docker install

dnf를 사용하도록 설정한다.    
```
dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
```

dnf를 이용하여 docker를 설치한다.   
```
dnf install docker-ce docker-ce-cli containerd.io
```

설치된 docker가 서버가 재부팅 되어도 실행되도록 시스템에 등록한다.   
```
systemctl enable --now docker
```

설치된 docker를 확인한다.
```
[root@virtualserver01 ~]# systemctl status docker
● docker.service - Docker Application Container Engine
   Loaded: loaded (/usr/lib/systemd/system/docker.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-06-28 00:49:02 CDT; 8s ago
     Docs: https://docs.docker.com
 Main PID: 54224 (dockerd)
    Tasks: 19
   Memory: 55.9M
   CGroup: /system.slice/docker.service
           └─54224 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

```

### 방화벽 설정 
방화벽 설정을 한다.
```
firewall-cmd --zone=public --add-masquerade --permanent
```
설정된 정보를 적용한다. 
```
firewall-cmd --reload
```

### docker-compose install
curl을 이용하여 docker-compose를 설치한다.   
```
curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

### docker 권한 설정 
docker-compose를 다른 계정에서 사용하도록 권한을 부여한다.   
```
chmod +x /usr/local/bin/docker-compose
```
일반 사용자가 docker를 사용할 수 있도록 docker 그룹을 추가한다.   
이후 생성된 계정을 docker그룹에 포함 시킨다.  
```
groupadd docker
```


### sample docker app 실행 
설치된 docker가 잘 동작하는지 확인한다.  
```
docker run hello-world
```
다음과 같은 메시지가 나오면 성공적으로 설치가 완료 되었다.    
```
[root@virtualserver01 ~]# docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
b8dfde127a29: Pull complete 
Digest: sha256:9f6ad537c5132bcce57f7a0a20e317228d382c3cd61edae14650eec68b2b345c
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/


```

# 계정 추가하기 
작업용 계정을 생성한다.  
```
useradd lab99
passwd lab99
```

생성된 docker 그룹에 ta 계정을 추가한다.    
```
usermod -aG docker lab02
```

docker login 안될 경우
```
setfacl --modify user:lab99:rw /var/run/docker.sock
```

# git install
dnf를 이용하여 git을 설치한다.   
```
dnf install git -y
dnf install unzip -y
dnf install maven -y
```


# ibm cloud cli 설치하기
### ibmcloud 설치하기 
https://cloud.ibm.com/docs/cli?topic=cli-install-ibmcloud-cli 에서 설치 스큽립트 복사한다.   

root 계정에서 설치한다.   
```
curl -fsSL https://clis.cloud.ibm.com/install/linux | sh
```

최신 소스로 업데이트 한다.
```
ibmcloud update
```

### iks를 이용하기 위해 Plug-in 설치하기 
k8s와 이미지 저장소를 사용하기 위해 플러그인 설치

https://cloud.ibm.com/docs/containers?topic=containers-cs_cli_install#cs_cli_install_steps

```
ibmcloud plugin install container-service
ibmcloud plugin install container-registry
ibmcloud plugin install observe-service
ibmcloud plugin list
```

### kubectl 설치하기
kubectl 설치하는 사이트  
https://cloud.ibm.com/docs/containers?topic=containers-cs_cli_install#kubectl

root 계정에서 kubectl 설치를 진행한다.
```  
wget  https://storage.googleapis.com/kubernetes-release/release/v1.20.7/bin/linux/amd64/kubectl
```


bin 디렉토리로 이동후 실행 권한을 준다. 
```
mv kubectl /usr/local/bin/kubectl
echo $PATH
chmod +x /usr/local/bin/kubectl

```


### imagepull secret 복사하기
default에 secret에 있는데 새로 추가한 namespace의경우 secret을 수동으로 추가해 주어야 한다. 
참고
https://cloud.ibm.com/docs/containers?topic=containers-registry#use_imagePullSecret
```
복사하기 , 시크릿 : all-icr-io , 네임스페이스 : lab99
kubectl get secret all-icr-io -n default -o yaml | sed 's/default/lab99-dev/g' | kubectl create -n lab99-dev -f -

서비스어카운트에 시크릿 추가하기, 이름이 : all-icr-io 인 경우 
kubectl patch -n lab99-dev serviceaccount/default -p '{"imagePullSecrets":[{"name": "all-icr-io"}]}'

이미 있는 경우
kubectl patch -n lab99 serviceaccount/default --type='json' -p='[{"op":"add","path":"/imagePullSecrets/-","value":{"name":"all-icr-io"}}]'



```
   
확인하기    
kubectl describe serviceaccount default
``` 
Name:                default
Namespace:           lab99
Labels:              <none>
Annotations:         <none>
Image pull secrets:  all-icr-io
                     ibmcloud-toolchain-f8732b6c-b0a9-42f7-8f88-5431725ccaee-jp.icr.io
Mountable secrets:   default-token-fc2sq
Tokens:              default-token-fc2sq
Events:              <none>
```


kubectl edit serviceaccount default
```
apiVersion: v1
kind: ServiceAccount
metadata:
  name: default
  namespace: lab99
secrets:
- name: default-token-fc2sq
imagePullSecrets:
- name: all-icr-io
- name: ibmcloud-toolchain-f8732b6c-b0a9-42f7-8f88-5431725ccaee-jp.icr.io
```



## 에러
```
[lab03@169 ~]$ ibmcloud ks cluster config --cluster c4fkkbpt07gmpr7kat00
FAILED
'ks' is not a registered command. See 'ibmcloud help'.
```

플러그인을 설치 해야함
```
ibmcloud plugin install container-service
ibmcloud plugin install container-registry
ibmcloud plugin install observe-service
ibmcloud plugin list
```


ibmcloud plugin install container-service
ibmcloud plugin install container-registry

kubectl create namespace lab90
kubectl config set-context --current --namespace=lab90

kubectl apply -f deployment.yaml

kubectl get pod